// routes/register.js
var express = require('express');
var router = express.Router();

let Utilisateur = require('../private/models/Utilisateur');

let secureInputs = (data) => {
  Object.keys(data).forEach(
    (key) => (data[key] = data[key].replace(/<.*?>|select|from|where|update|insert/g, ''))
  );
};

router.get('/register', function (req, res) {
  res.render('index');
});

router.post('/register', function (req, res) {
  secureInputs(req.body);
  Utilisateur.register(req.body.username, req.body.password);
  res.render('index');
});

module.exports = router;