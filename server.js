// server.js
const express = require('express');
const app = express();
const port = 3000;

// Configurer EJS comme moteur de modèle
app.set('view engine', 'ejs');

// Middleware pour parser le corps des requêtes POST
app.use(express.urlencoded({ extended: true }));

// Utiliser les routes
const registerRouter = require('./routes/register');
app.use('/', registerRouter);

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Serveur écoutant sur le port ${port}`);
});