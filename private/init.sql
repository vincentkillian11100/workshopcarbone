DROP DATABASE IF EXISTS workShop ;
CREATE DATABASE workShop;
USE workShop;
GRANT ALL PRIVILEGES ON workShop.* TO ''@'';
CREATE TABLE Acteurs (
  acteur_id INT PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255),
  role VARCHAR(10),
  nom VARCHAR(255),
  secteur_activite VARCHAR(255),
  adresse VARCHAR(255)
);

CREATE TABLE Energie (
  energie_id INT PRIMARY KEY,
  type_energie VARCHAR(255) NOT NULL,
  quantite_consommee INT,
  date_consommation DATE,
  acteur_id INT,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE Repartition (
  repartition_id INT PRIMARY KEY,
  acteur_id INT,
  salle VARCHAR(255),
  surface INT,
  nombre_salaries INT,
  nombre_voitures INT,
  deplacements_train INT,
  deplacements_voiture INT,
  deplacements_velo INT,
  electromenager INT,
  pc_ordinateurs INT,
  tv INT,
  salle_jeu INT,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE Solutions (
  solution_id INT PRIMARY KEY,
  description TEXT
);

CREATE TABLE Sensibilisation (
  sensibilisation_id INT PRIMARY KEY,
  acteur_id INT,
  type_consequence VARCHAR(255) NOT NULL,
  description TEXT,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE OffrePremium (
  offre_id INT PRIMARY KEY,
  acteur_id INT,
  type_offre VARCHAR(255) NOT NULL,
  description TEXT,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE Articles (
  article_id INT PRIMARY KEY,
  acteur_id INT,
  nom_article VARCHAR(255) NOT NULL,
  description TEXT,
  quantite_stock INT,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE Consommation (
  consommation_id INT PRIMARY KEY,
  article_id INT,
  acteur_id INT,
  date_consommation DATE,
  quantite_consommee INT,
  FOREIGN KEY (article_id) REFERENCES Articles(article_id),
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);

CREATE TABLE Articles_Solutions (
  article_id INT,
  solution_id INT,
  PRIMARY KEY (article_id, solution_id),
  FOREIGN KEY (article_id) REFERENCES Articles(article_id),
  FOREIGN KEY (solution_id) REFERENCES Solutions(solution_id)
);

CREATE TABLE Acteurs_Solutions (
  acteur_id INT,
  solution_id INT,
  PRIMARY KEY (acteur_id, solution_id),
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id),
  FOREIGN KEY (solution_id) REFERENCES Solutions(solution_id)
);

CREATE TABLE BilanTrimestriel (
  bilan_id INT PRIMARY KEY,
  acteur_id INT,
  trimestre INT NOT NULL,
  annee INT NOT NULL,
  baisse_empreinte INT,
  avantages TEXT,
  amelioration_percent FLOAT NOT NULL,
  FOREIGN KEY (acteur_id) REFERENCES Acteurs(acteur_id)
);